# README #

To build screw'd 7.x

Initializing Repo

# Create a directory for the source files
# This can be located anywhere (as long as the fs is case-sensitive)
$ mkdir WORKSPACE
$ cd WORKSPACE

# Install Repo in the created directory
$ repo init -u https://github.com/ScrewdAOSP/platform_manifest -b n7x

Downloading the source tree

This is what you will run each time you want to pull in upstream changes. Keep in mind that on your first run, it is expected to take a while as it will download all the required Android source files and their change histories.

# Let Repo take care of all the hard work
$ repo sync -j4
Building the entire ROM

The bundled builder tool ./rom-build.shhandles all the building steps for the specified device automatically. As the device value, you just feed it with the device codename (for example, 'angler' for the Nexus 6p).

# Go to the root of the source tree...
$ cd WORKSPACE
# ...and run the builder tool.
$ ./rom-build.sh DEVICE
# So if you want to build for the Nexus 6p, run:
$ ./rom-build.sh angler
If all goes well, you will see a message showing where the completed build is located.





